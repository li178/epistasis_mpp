# test the three simQTLs in rP and cP&F (1-DGS approach) 
# and summarize the results of 500 replications
# this scripts is for 'source()' from example4simMPP.R

parents <- names(parGeno)[-c(1:3)]
nloc <- nrow(map)
megaM <- design.HapM(progHap,parents,1:nloc)
threshold <- -log10(0.01)
sel.markers <- c(57,266,440)
AIC_threshold <- 0

allepisRes <- list()
allrefRes <- list()

for ( j in 1:length(names(allprogPheno))) {
  
  print(paste0('for set:',j))
  allprogPheno[[paste0('set',j)]]$param #check paramters of this set
  progPheno <- allprogPheno[[paste0('set',j)]]$progPheno# the simY from this set
  progPheno$pop <- factor(progPheno$pop)
  
  #rP (reference model with only parent-specific QTLs)————————————————————————————————————————————————————————————————————————————————————————----
  selcof <- NULL
  minlog10p <- c()
  refRes <- data.frame(simQTL=paste0('simQTL',1:3),sel.markers=sel.markers, cofactor=FALSE)
  
  for (r in 1:4) { # for three simQTLs, maximum all three pass the test so maximum loop four times
    print(r)
    for (m in 1:length(sel.markers)) {
      df2 <- refRes[-m,]
      selcof <- df2$sel.markers[which(df2$cofactor)]
      if (length(selcof)==0) {
        selcof <- NULL
      }
      obj1 <- rPmodel(parGeno,megaM,progPheno, trait.name='simY', scan_pos=sel.markers[m],cof=selcof,str.residual='hete',null.model=F)
      obj0 <- rPmodel(parGeno,megaM,progPheno, trait.name='simY', scan_pos=sel.markers[m],cof=selcof,str.residual='hete',null.model=T)
      
      minlog10p[m] <- lrt.minlog10p(obj0$asreml.obj, obj1$asreml.obj)
    }
    refRes$minlog10p <- minlog10p
    if (length(which(refRes$cofactor))==0) {
      if (max(minlog10p)<threshold) break
      cof.ind <- which(minlog10p==max(minlog10p))
      refRes$cofactor[cof.ind]<- TRUE
    } else {
      if (length(which(refRes$cofactor))==3) break
      if (max(minlog10p[-which(refRes$cofactor)])<threshold) break
      cof <- which(minlog10p==max(minlog10p[-which(refRes$cofactor)]))
      refRes$cofactor[cof]<-TRUE
    }
  }
  refRes$set=j
  
  allrefRes[[j]] <- refRes
  
  #cP&F (the 1-D genome scan model with combined parent-specific and family-specific QTLs)————————————————————————————————————————————————————————————————————————————————————————----
  selcof <- NULL
  minlog10p <- c()
  type <- c()
  AIC.diff <- c()
  episRes <- data.frame(simQTL=paste0('simQTL',1:3),sel.markers=sel.markers,
                      cofactor=FALSE,cofactor.defin='unknown')
  for (r in 1:4) {
    print(r)
    for (m in 1:length(sel.markers)) {
      df2  <- episRes[-m,]
      selcof <- df2$sel.markers[which(df2$cofactor)]
      definselCof <- df2$cofactor.defin[which(df2$cofactor)]
      if (length(selcof)==0) {
        selcof <- NULL
        definselCof <- NULL
      }
      
      obj_parent <- cPFmodel(trait.name='simY',
                             scan_pos=sel.markers[m],
                             definScan='parent',
                             cof=selcof,
                             definCof=definselCof,
                             parGeno,
                             progPheno,
                             megaM,
                             str.residual='hete',
                             null.model=FALSE)
                                    
      
      obj_family <- cPFmodel(trait.name='simY',
                             scan_pos=sel.markers[m],
                             definScan='family',
                             cof=selcof,
                             definCof=definselCof,
                             parGeno,
                             progPheno,
                             megaM,
                             str.residual='hete',
                             null.model=FALSE)
                                    
      
      obj_null <- cPFmodel(trait.name='simY',
                           scan_pos=sel.markers[m],
                           definScan='family',
                           cof=selcof,
                           definCof=definselCof,
                           parGeno,
                           progPheno,
                           megaM,
                           str.residual='hete',
                           null.model=TRUE)

      minlog10p_main_par <- lrt.minlog10p(obj_null$asreml.obj, obj_parent$asreml.obj)
      minlog10p_main_fam <- lrt.minlog10p(obj_null$asreml.obj, obj_family$asreml.obj)

      IC.family <- infoCriteria.asreml(obj_family$asreml.obj)
      IC.parent <- infoCriteria.asreml(obj_parent$asreml.obj)
      
      (AIC_epis <- (IC.parent$AIC-IC.family$AIC))
      AIC.diff[m] <- AIC_epis
      if(AIC_epis< AIC_threshold) {
        minlog10p[m] <- minlog10p_main_par
        type[m] <- 'parent'
      } else {
        minlog10p[m] <- minlog10p_main_fam
        type[m] <- 'family'
      }
      
    }
    episRes$AIC.diff <- AIC.diff
    episRes$minlog10p <- minlog10p
    episRes$cofactor.defin <- type
    if (length(which(episRes$cofactor))==0) {
      if (max(minlog10p)<threshold) break
      cof.ind <- which(minlog10p==max(minlog10p))
      episRes$cofactor[cof.ind] <- TRUE
    } else {
      if (length(which(episRes$cofactor))==3) break
      if (max(minlog10p[-which(episRes$cofactor)])<threshold) break
      cof <- which(minlog10p==max(minlog10p[-which(episRes$cofactor)]))
      episRes$cofactor[cof] <- TRUE
    }
  }
  
  episRes$set <- j
  allepisRes[[j]] <- episRes
  
}

allepisRes <- do.call('rbind',allepisRes)
allrefRes <- do.call('rbind',allrefRes)

#summary----
head(allrefRes)
head(allepisRes)
threshold
# maxepis=0
# rep.n=500
set.n <- length(names(allprogPheno))/rep.n
setRes <- list()
setnQTLsum <- list()

for (s in 1:set.n) {
  
  set1refRes <- allrefRes %>% filter(set %in% ((s-1)*rep.n+1):(s*rep.n)) 
  set1Res <- allepisRes %>% filter(set %in% ((s-1)*rep.n+1):(s*rep.n)) %>% 
    mutate(ref.minlog10p=set1refRes$minlog10p,epis=paste0('episSet',s))
  nQTLsum <- list()
  for (q in 1:3){
    #by cP&F model
    episModQTL <- set1Res%>% filter(simQTL==paste0('simQTL',q),minlog10p>threshold) 
    episModQTL_f <- episModQTL%>% filter(cofactor.defin=='family')
    episModQTL_p <- episModQTL%>% filter(cofactor.defin=='parent')
    nrow(episModQTL_p)
    #by rP model
    refModQTL <- set1Res%>% filter(simQTL==paste0('simQTL',q),ref.minlog10p>threshold) 
    
    nQTL <- data.frame(simQTL=paste0('simQTL',q),
                     mod=c('cPF','cPF','rP'),
                     nQTL=c(nrow(episModQTL_f),nrow(episModQTL_p), nrow(refModQTL)),
                     defin=c('cPF:family','cPF:parent','rP'),
                     epis=paste0('epis:',c(0,0.1,0.2,0.3)[s]))

    nQTLsum[[q]] <- nQTL
    
    
  }
  
  setRes[[s]] <- set1Res
  setnQTLsum[[s]] <- do.call('rbind',nQTLsum)
  

}

Res <- do.call('rbind',setRes)
allnQTLsum <- do.call('rbind',setnQTLsum)

allnQTLsum$powerQTL <- allnQTLsum$nQTL/rep.n #transfer to percentages
sumBar <- ggplot(allnQTLsum, aes(x=epis,y=powerQTL,fill=defin)) +
  geom_bar(stat = "identity",width = 0.3)+
  scale_fill_manual(values=c("coral2", "cyan3", "grey60"))+
  ylim(0,1)+
  ylab('Power')+
  xlab('Interaction effect between simQTL1 and simQTL3')+
  facet_grid(mod~simQTL)+
  theme(
    panel.grid.major = element_blank(),
    panel.grid.minor = element_blank(),
    plot.title = element_text(hjust = 0.5),
    axis.text.x = element_text(angle = 90),
    panel.background = element_blank(),
    plot.background = element_blank(),
    legend.position = "top",
    strip.background = element_blank(),
    panel.border = element_rect(fill = NA, color = "black",size = 0.5, linetype = "solid"))

