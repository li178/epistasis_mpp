# simulated example for analysis in:
# "A One-dimension Genome Scan Using the Mixed Model Approach Reveals QTL-by-genetic-background Interaction in Diallel and Nested Association Mapping designs"

library(asreml)
library(asremlPlus)
library(psych)
library(dplyr)
library(tidyr)
library(ggplot2)
source('R/functions_asreml4.r')

# dir <- 'data/example_simNAM'
dir <- 'data/example_simDiallel'

#sim parameters----
rep.n=500 # replicates: using 100 as an example for time saving, can set a higher number like 500
a=0# additive effect for the 3 simQTLs
e13=rep(c(0,0.1,0.2,0.3),each=rep.n)#add-add interaction effects between 1st and 3rd QTLs, 500 rep for each
parammat <- t(data.frame(a1=a,a2=a,a3=a,e12=0,e13=e13,e23=0,e123=0))

#read data----
progenyfile <- read.csv(paste0(dir,'/progenyfile.csv'))
progenyfile$pop <- factor(progenyfile$pop)
parGeno <- read.csv(paste0(dir,'/parGeno.csv'),stringsAsFactors = F)
progHap <- read.csv(paste0(dir,'/progHap.csv'),stringsAsFactors = F)
map <- read.csv(paste0(dir,'/map.csv'),stringsAsFactors = F)
progIBS <- read.csv(paste0(dir,'/progIBS.csv'),stringsAsFactors = F)

#sim phenotypes and analysis----
QTLnames=c('M57','M266','M440') #select three positions for analysis
set.seed(1234)
allprogPheno <- simPheno(progIBS,QTLnames,parammat)
source('R/testEpisQTL_MPP.r')
allnQTLsum
sumBar

#save----
save(list = ls(),file=paste0(dir,'/simDiallel_a0_rep500.rdata'))
